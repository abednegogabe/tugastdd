package tugas;

import jdk.internal.jline.internal.ShutdownHooks;

import java.util.ArrayList;

public class Todolist {
    ArrayList<Task> taskList = new ArrayList<>();

    public void addTask(int id, String name, String status) {
        Task task = new Task();
        task.setTask(id, name, status);
        taskList.add(task);
    }

    public String getTask(){
        String result = "";
        for(int i=0; i<taskList.size();i++){
            result += taskList.get(i).getTask();
            if(i<taskList.size()-1) {
                result += "\n";
            }
        }
        return result;
    }
}



