package tugas;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void testGetTask(){
        Task task = new Task();
        int id = 1;
        String name = "Test";
        String status = "DONE";
        task.setTask(id,name,status);
        assertEquals("1. Test [DONE]",task.getTask());
    }
    @Test
    public void testGetTaskOnNullReferences(){
        Task task = new Task();
        assertEquals("no task",task.getTask());
    }
}

