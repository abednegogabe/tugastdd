package tugas;

import org.junit.Test;

import static org.junit.Assert.*;

public class TodolistTest {
    @Test
    public void testAddTask() {
        Todolist todo = new Todolist();
        todo.addTask(1, "Test", "DONE");
        assertEquals("1. Test [DONE]", todo.taskList.get(0).getTask());
    }

    @Test
    public void testTaskList() {
        Todolist todo = new Todolist();
        todo.addTask(1, "Test", "DONE");
        todo.addTask(2, "Learn Java", "NOT DONE");
        todo.addTask(3, "Learn TDD", "NOT DONE");
        assertEquals("1. Test [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]", todo.getTask());
    }
}